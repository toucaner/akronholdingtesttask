﻿namespace AkronHoldingTestTask.Core.Domain
{
    public class CarProperty : EntityBase
    {
        #region Properties

        public string Value { get; set; }

        public Car Car { get; set; }

        #endregion
    }
}