﻿namespace AkronHoldingTestTask.Core.Domain
{
    public class Car : EntityBase
    {
        #region Properties

        public Garage Garage { get; set; }

        #endregion
    }
}