﻿using System.ComponentModel.DataAnnotations;

namespace AkronHoldingTestTask.Core.Domain
{
    public class EntityBase
    {
        #region Properties

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}