﻿using AkronHoldingTestTask.Core.Domain;
using AkronHoldingTestTask.Core.Dto;
using AutoMapper;

namespace AkronHoldingTestTask.Core.Mapping
{
    public class MappingProfile : Profile
    {
        #region Constructor

        public MappingProfile()
        {
            CreateMap<Garage, GarageDto>();
            CreateMap<GarageDto, Garage>();
            CreateMap<Car, CarDto>()
                .ForMember("GarageId", opt => opt.MapFrom(src => src.Garage.Id))
                .ForMember("GarageName", opt => opt.MapFrom(src => src.Garage.Name));
            CreateMap<CarProperty, CarPropertyDto>()
                .ForMember("CarId", opt => opt.MapFrom(src => src.Car.Id))
                .ForMember("CarName", opt => opt.MapFrom(src => src.Car.Name));
        }

        #endregion
    }
}