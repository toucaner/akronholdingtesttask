﻿namespace AkronHoldingTestTask.Core.Dto
{
    public class GarageDto
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}