﻿namespace AkronHoldingTestTask.Core.Dto
{
    public class CarPropertyDto
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public int CarId { get; set; }

        public string CarName { get; set; }

        #endregion
    }
}