﻿namespace AkronHoldingTestTask.Core.Dto
{
    public class CarDto
    {
        #region Properties

        public int Id { get; set; }

        public string Name { get; set; }

        public int GarageId { get; set; }

        public string GarageName { get; set; }

        #endregion
    }
}