﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;

namespace AkronHoldingTestTask.Services
{
    public interface IService<T> where T : EntityBase
    {
        #region Methods

        Task<T> GetAsync(int id);

        Task<List<T>> GetAllAsync();

        Task<int> InsertAsync(T entity);

        Task<int> UpdateAsync(T entity);

        Task<int> DeleteAsync(T entity);

        #endregion
    }
}