﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Data.Repository;

namespace AkronHoldingTestTask.Services.Car
{
    public class CarService : ICarService
    {
        #region Fields

        private readonly IRepository<Core.Domain.Car> _carRepository;

        #endregion

        #region Constructor

        public CarService(IRepository<Core.Domain.Car> carRepository)
        {
            _carRepository = carRepository;
        }

        #endregion

        #region Methods

        public Task<Core.Domain.Car> GetAsync(int id)
        {
            return _carRepository.GetWithRelatedAsync(id, nameof(Core.Domain.Garage));
        }

        public Task<List<Core.Domain.Car>> GetAllAsync()
        {
            return _carRepository.GetAllWithRelatedAsync(nameof(Core.Domain.Garage));
        }

        public Task<int> InsertAsync(Core.Domain.Car entity)
        {
            return _carRepository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(Core.Domain.Car entity)
        {
            return _carRepository.UpdateAsync(entity);
        }

        public Task<int> DeleteAsync(Core.Domain.Car entity)
        {
            return _carRepository.DeleteAsync(entity);
        }

        #endregion
    }
}