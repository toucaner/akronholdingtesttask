﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkronHoldingTestTask.Data.Repository;

namespace AkronHoldingTestTask.Services.CarProperty
{
    public class CarPropertyService : ICarPropertyService
    {
        #region Fields

        private readonly IRepository<Core.Domain.CarProperty> _carPropertyRepository;

        #endregion

        #region Constructor

        public CarPropertyService(IRepository<Core.Domain.CarProperty> carPropertyRepository)
        {
            _carPropertyRepository = carPropertyRepository;
        }

        #endregion

        #region Methods

        public Task<Core.Domain.CarProperty> GetAsync(int id)
        {
            return _carPropertyRepository.GetWithRelatedAsync(id, nameof(Core.Domain.Car));
        }

        public Task<List<Core.Domain.CarProperty>> GetAllAsync()
        {
            return _carPropertyRepository.GetAllWithRelatedAsync(nameof(Core.Domain.Car));
        }

        public Task<int> InsertAsync(Core.Domain.CarProperty entity)
        {
            return _carPropertyRepository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(Core.Domain.CarProperty entity)
        {
            return _carPropertyRepository.UpdateAsync(entity);
        }

        public Task<int> DeleteAsync(Core.Domain.CarProperty entity)
        {
            return _carPropertyRepository.DeleteAsync(entity);
        }

        #endregion
    }
}