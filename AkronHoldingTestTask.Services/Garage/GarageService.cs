﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Data.Repository;

namespace AkronHoldingTestTask.Services.Garage
{
    public class GarageService : IGarageService
    {
        #region Fields

        private readonly IRepository<Core.Domain.Garage> _garageRepository;

        #endregion

        #region Constructor

        public GarageService(IRepository<Core.Domain.Garage> garageRepository)
        {
            _garageRepository = garageRepository;
        }

        #endregion

        #region Methods

        public Task<Core.Domain.Garage> GetAsync(int id)
        {
            return _garageRepository.GetAsync(id);
        }

        public Task<List<Core.Domain.Garage>> GetAllAsync()
        {
            return _garageRepository.GetAllAsync();
        }

        public Task<int> InsertAsync(Core.Domain.Garage entity)
        {
            return _garageRepository.InsertAsync(entity);
        }

        public Task<int> UpdateAsync(Core.Domain.Garage entity)
        {
            return _garageRepository.UpdateAsync(entity);
        }

        public Task<int> DeleteAsync(Core.Domain.Garage entity)
        {
            return _garageRepository.DeleteAsync(entity);
        }

        #endregion
    }
}