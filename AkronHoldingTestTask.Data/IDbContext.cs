﻿using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace AkronHoldingTestTask.Data
{
    public interface IDbContext
    {
        #region Methods

        DbSet<T> Set<T>() where T : EntityBase;

        Task<int> SaveChangesAsync();

        #endregion
    }
}
