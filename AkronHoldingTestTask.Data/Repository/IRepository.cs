﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;

namespace AkronHoldingTestTask.Data.Repository
{
    public interface IRepository<T> where T : EntityBase
    {
        #region Methods

        Task<T> GetAsync(int id);

        Task<T> GetWithRelatedAsync(int id, string related);

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAllWithRelatedAsync(string related);

        Task<int> InsertAsync(T entity);

        Task<int> UpdateAsync(T entity);

        Task<int> DeleteAsync(T entity);

        #endregion
    }
}