﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace AkronHoldingTestTask.Data.Repository
{
    public class EfRepository<T> : IRepository<T> where T : EntityBase
    {
        #region ctor

        public EfRepository(IDbContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }

        #endregion

        #region Fields

        private readonly IDbContext _context;

        private readonly DbSet<T> _entities;

        #endregion

        #region Methods

        public Task<T> GetAsync(int id)
        {
            return _entities.FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public Task<T> GetWithRelatedAsync(int id, string related)
        {
            return _entities.Include(related).FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public Task<List<T>> GetAllAsync()
        {
            return _entities.OrderBy(entity => entity.Id).ToListAsync();
        }

        public Task<List<T>> GetAllWithRelatedAsync(string related)
        {
            return _entities.Include(related).OrderBy(entity => entity.Id).ToListAsync();
        }

        public Task<int> InsertAsync(T entity)
        {
            _entities.Add(entity);
            return _context.SaveChangesAsync();
        }

        public Task<int> UpdateAsync(T entity)
        {
            return _context.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(T entity)
        {
            _entities.Remove(entity);
            return _context.SaveChangesAsync();
        }

        #endregion
    }
}