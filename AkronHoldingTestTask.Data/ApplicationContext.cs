﻿using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace AkronHoldingTestTask.Data
{
    public class ApplicationContext : DbContext, IDbContext
    {
        #region Constructor

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        #endregion

        #region Properties

        public DbSet<Garage> Garages { get; set; }

        public DbSet<Car> Cars { get; set; }

        public DbSet<CarProperty> CarProperties { get; set; }

        #endregion

        #region Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer("Data Source=Victor;Initial Catalog=AkronHoldingTestTask;Integrated Security=True;");
        }

        public new DbSet<T> Set<T>() where T : EntityBase
        {
            return base.Set<T>();
        }

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        #endregion
    }
}