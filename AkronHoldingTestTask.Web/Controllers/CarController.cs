﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using AkronHoldingTestTask.Core.Dto;
using AkronHoldingTestTask.Services.Car;
using AkronHoldingTestTask.Services.Garage;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AkronHoldingTestTask.Web.Controllers
{
    public class CarController : Controller
    {
        #region Fields

        private readonly ICarService _carService;

        private readonly IGarageService _garageService;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public CarController(IMapper mapper, 
            ICarService carService, 
            IGarageService garageService)
        {
            _mapper = mapper;
            _carService = carService;
            _garageService = garageService;
        }

        #endregion

        #region Methods

        public ActionResult Index()
        {
            return View();
        }

        public async Task<string> GetData()
        {
            var result = await _carService.GetAllAsync();
            var dtos = _mapper.Map<IList<CarDto>>(result);

            return JsonConvert.SerializeObject(dtos);
        }

        [HttpPost]
        public async Task<JsonResult> Save(CarDto record)
        {
            try
            {
                var car = new Car();
                var garage = await _garageService.GetAsync(record.GarageId);

                if (record.Id > 0)
                {
                    car = await _carService.GetAsync(record.Id);
                    if (car == null)
                        return Json(new { result = false });

                    car.Name = record.Name;
                    car.Garage = garage;

                    await _carService.UpdateAsync(car);
                }
                else
                {
                    car.Name = record.Name;
                    car.Garage = garage;

                    await _carService.InsertAsync(car);
                }

                return Json(new { result = true });
            }
            catch (Exception)
            {
                return Json(new { result = false });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            var car = await _carService.GetAsync(id);
            if (car == null)
                return Json(new { result = false });

            await _carService.DeleteAsync(car);

            return Json(new { result = true });
        }

        #endregion
    }
}