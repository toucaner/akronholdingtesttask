﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using AkronHoldingTestTask.Core.Dto;
using AkronHoldingTestTask.Services.Car;
using AkronHoldingTestTask.Services.CarProperty;
using AkronHoldingTestTask.Services.Garage;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AkronHoldingTestTask.Web.Controllers
{
    public class CarPropertyController : Controller
    {
        #region Fields

        private readonly ICarService _carService;

        private readonly ICarPropertyService _carPropertyService;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public CarPropertyController(IMapper mapper,
            ICarService carService,
            ICarPropertyService carPropertyService)
        {
            _mapper = mapper;
            _carService = carService;
            _carPropertyService = carPropertyService;
        }

        #endregion

        #region Methods

        public ActionResult Index()
        {
            return View();
        }

        public async Task<string> GetData()
        {
            var result = await _carPropertyService.GetAllAsync();
            var dtos = _mapper.Map<IList<CarPropertyDto>>(result);

            return JsonConvert.SerializeObject(dtos);
        }

        [HttpPost]
        public async Task<JsonResult> Save(CarPropertyDto record)
        {
            try
            {
                var carProperty = new CarProperty();
                var car = await _carService.GetAsync(record.CarId);

                if (record.Id > 0)
                {
                    carProperty = await _carPropertyService.GetAsync(record.Id);
                    if (carProperty == null)
                        return Json(new { result = false });

                    carProperty.Name = record.Name;
                    carProperty.Value = record.Value;
                    carProperty.Car = car;

                    await _carPropertyService.UpdateAsync(carProperty);
                }
                else
                {
                    carProperty.Name = record.Name;
                    carProperty.Value = record.Value;
                    carProperty.Car = car;

                    await _carPropertyService.InsertAsync(carProperty);
                }

                return Json(new { result = true });
            }
            catch (Exception)
            {
                return Json(new { result = false });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            var carProperty = await _carPropertyService.GetAsync(id);
            if (carProperty == null)
                return Json(new { result = false });

            await _carPropertyService.DeleteAsync(carProperty);

            return Json(new { result = true });
        }

        #endregion
    }
}