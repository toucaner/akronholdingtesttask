﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AkronHoldingTestTask.Core.Domain;
using AkronHoldingTestTask.Core.Dto;
using AkronHoldingTestTask.Services.Garage;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AkronHoldingTestTask.Web.Controllers
{
    public class GarageController : Controller
    {
        #region Fields

        private readonly IGarageService _garageService;

        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        public GarageController(IMapper mapper, IGarageService garageService)
        {
            _mapper = mapper;
            _garageService = garageService;
        }

        #endregion

        #region Methods

        public ActionResult Index()
        {
            return View();
        }

        public async Task<string> GetData()
        {

            var result = await _garageService.GetAllAsync();
            var dtos = _mapper.Map<IList<GarageDto>>(result);

            return JsonConvert.SerializeObject(dtos);
        }

        [HttpPost]
        public async Task<JsonResult> Save(Garage record)
        {
            try
            {
                if (record.Id > 0)
                {
                    var garage = await _garageService.GetAsync(record.Id);
                    if (garage == null)
                        return Json(new { result = false });

                    garage.Name = record.Name;

                    await _garageService.UpdateAsync(record);
                }
                else
                {
                    await _garageService.InsertAsync(record);
                }

                return Json(new {result = true});
            }
            catch (Exception)
            {
                return Json(new {result = false});
            }
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            var garage = await _garageService.GetAsync(id);
            if (garage == null)
                return Json(new {result = false});

            await _garageService.DeleteAsync(garage);

            return Json(new {result = true});
        }

        #endregion
    }
}